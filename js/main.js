var mainWrapper = document.querySelector('.main_wrapper');
var invoiceWrapper = document.querySelector('.invoice-wrapper');

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        document.getElementById('error').style.display = 'none';
        return true;
    }
}

function downloadPDF() {
    const element = document.getElementById("invoice");
    html2pdf()
        .set({ html2canvas: { scale: 4 }, filename: "excitel-broadband-invoice.pdf" })
        .from(element)
        .save();
};

function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
};

function hideErr() {
    document.getElementById('error').style.display = 'none';
};

function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("date").value = today;
}


window.onload = function () {
    getDate();
};

function previewInvoice() {
    var name = document.getElementById('name').value;
    var house = document.getElementById('house').value;
    var floor = document.getElementById('floor-no').value;
    var area = document.getElementById('area').value;
    var city = document.getElementById('city').value;
    var state = document.getElementById('state').value;
    var pin = document.getElementById('pin').value;
    var amount = parseInt(document.getElementById('amount').value);
    var amountVal = document.getElementById('amount').value;
    var mobile = document.getElementById('mobile').value;
    var date = document.getElementById('date').value;
    var startdate = document.getElementById('start_date').value;
    var enddate = document.getElementById('end_date').value;
    if (name.length && house.length && floor.length && area.length && city.length && state.length && pin.length && amountVal.length && mobile.length && date.length && startdate.length && enddate.length) {
        // Invoice Name
        document.getElementById('invoice-name').innerText = titleCase(name);

        //Invoice Number
        var inv = document.getElementById('inv-no');
        var twoDigit = Math.floor(Math.random() * 90 + 10);
        var nineDigit = Math.floor(Math.random() * 900000000 + 100000000);
        var invoiceNumber = `KA${twoDigit}TRI${nineDigit}`;
        inv.innerText = invoiceNumber;

        // House
        document.getElementById('house-no').innerText = titleCase(house);

        // floor
        document.getElementById('floor').innerText = titleCase(floor);

        // Area
        document.getElementById('area-sec').innerText = titleCase(area);

        // City
        document.getElementById('city-sec').innerText = titleCase(city);

        // State
        document.getElementById('state-sec').innerText = titleCase(state);

        // Pin
        document.getElementById('pin-sec').innerText = titleCase(pin);

        // Invoice Date
        var invoiceDate = date.split('-').reverse().join('/');
        var invoiceStartDate = startdate.split('-').reverse().join('/');
        var invoiceEndDate = enddate.split('-').reverse().join('/');
        document.getElementById('date-sec').innerText = invoiceDate;
        document.getElementById('start_date_n').innerText = invoiceStartDate;
        document.getElementById('end_date_n').innerText = invoiceEndDate;

        // Amount 
        var baseAmount = parseFloat(100 / 118 * amount);
        var gst = parseFloat(9 / 118 * amount);
        document.getElementById('base-amount').innerText = baseAmount.toFixed(2);
        document.getElementById('sgst').innerText = gst.toFixed(2);
        document.getElementById('cgst').innerText = gst.toFixed(2);
        document.getElementById('total-amount').innerText = amount.toFixed(2);

        // Mobile
        document.getElementById('mobile-sec').innerText = mobile;

        // Digit to Word
        var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
        var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        function inWords(num) {
            if ((num = num.toString()).length > 9) return 'overflow';
            n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
            if (!n) return; var str = '';
            str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
            str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
            str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
            str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
            str += (n[5] != 0) ? ((str != '') ? '' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : 'only';
            return str;
        }

        mainWrapper.style.display = 'none';
        invoiceWrapper.style.display = 'block';
        document.getElementById('word').innerText = inWords(amount).toUpperCase();
        if (amount <= 4000) {
            document.getElementById('speed').innerText = '200';
        } else if (amount > 4000) {
            document.getElementById('speed').innerText = '300';
        };
    } else {
        document.getElementById('error').style.display = 'block';
    }
    window.scrollTo(0, 0); 
}

function backToHome() {
    invoiceWrapper.style.display = 'none';
    mainWrapper.style.display = 'block';
    window.scrollTo(0, 0);
}